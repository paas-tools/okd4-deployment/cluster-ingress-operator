# CERN modifications

Openshift's ingress operator gives us very little flexibility for configuration the HAProxy router deployments and associated services.

One issue in particular made it necessary to fork the upstream component and add our own modifications: the ability to modify Loadbalancer services created by the operator (we need to set specific annotations for Openstack) and the ability to selectively enable the PROXY protocol for specific router shards.
Details can be found in [okd4-install!703](https://gitlab.cern.ch/paas-tools/okd4-install/-/merge_requests/703).

The Gitlab repository is configured to automatically mirror the [upstream cluster-ingress-operator repository](https://github.com/openshift/cluster-ingress-operator).

Use the following workflow to port our custom patches to a new release:

```sh
# Make sure your repo is up-to-date
git fetch origin
# Check out the pristine upstream release
git checkout origin/release-4.x
# Push a new branch with the pristine upstream release
git checkout -b cern-4.x
git push origin cern-4.x
# Create a new branch for development
git checkout -b cern-4.x-dev
```

Then, locate the latest patches that have been made by us.
This can be done by checking out an old release branch (e.g. `cern-4.7`) and looking at the commits prefixed with `[cern]`.

```
git log origin/cern_cloud-4.7
git cherry-pick [CERN_PATCH_REFS]
```

At this point, you might need to resolve the merge conflicts (*fingers crossed*).
Afterwards, commit your changes with a `[cern]` prefix and push them to the dev branch.
Finally, create a merge request from `cern-4.x-dev` to `cern-4.x` so someone else can review your changes.

# OpenShift Ingress Operator

Ingress Operator is an [OpenShift](https://www.openshift.com) component which enables external access to cluster services by configuring Ingress Controllers, which route traffic as specified by OpenShift [Route](https://docs.openshift.com/container-platform/3.11/architecture/networking/routes.html) and Kubernetes [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) resources.

To provide this functionality, Ingress Operator deploys and manages an
[OpenShift router](https://github.com/openshift/router) — a
[HAProxy-based](https://www.haproxy.com) Kubernetes [ingress
controller](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers).

Ingress Operator implements the OpenShift [ingresscontroller API](https://github.com/openshift/api/blob/master/operator/v1/types_ingress.go).

## Installing

Ingress Operator is a core feature of OpenShift and is enabled out of the box.

Every new [OpenShift installation](https://github.com/openshift/installer)
 has an `ingresscontroller` named `default` which can be customized,
replaced, or supplemented with additional ingress controllers. To view the
default ingress controller, use the `oc` command:

```shell
$ oc describe --namespace=openshift-ingress-operator ingresscontroller/default
```

## Managing

Create and edit `ingresscontroller.operator.openshift.io` resources to manage
ingress controllers.

Interact with ingress controllers using the `oc` command. Every ingress
controller lives in the `openshift-ingress-operator` namespace.

To scale an ingress controller:

```shell
$ oc scale \
   --namespace=openshift-ingress-operator \
   --replicas=1 \
   ingresscontroller/<name>

$ oc patch \
   --namespace=openshift-ingress-operator \
   --patch='{"spec": {"replicas": 2}}' \
   --type=merge \
   ingresscontroller/<name>
```

**Note:** Using `oc scale` on an `ingresscontroller` where `.spec.replicas` is unset will currently return an error ([Kubernetes #75210](https://github.com/kubernetes/kubernetes/pull/75210)).

## Customizing

Create new `ingresscontroller` resources in the `openshift-ingress-operator`
namespace.

To edit an existing ingress controller:

```shell
$ oc edit --namespace=openshift-ingress-operator ingresscontroller/<name>
```

**Important:** Updating an ingress controller may lead to disruption for public
facing network connections as a new ingress controller revision may be rolled
out.

Refer to the [ingresscontroller API](https://github.com/openshift/api/blob/master/operator/v1/types_ingress.go) for full details on defaults and
customizing an ingress controller. The most important initial customizations are
domain and endpoint publishing strategy, as they *cannot currently be changed
after the ingress controller is created*.

### Endpoint publishing

The `.spec.endpointPublishingStrategy` field is used to publish the ingress
controller endpoints to other networks, enable load balancer integrations, etc.

Every strategy is described in detail in the [ingresscontroller API](https://github.com/openshift/api/blob/master/operator/v1/types_ingress.go). A brief
design diagram for each is shown below.

#### LoadBalancerService

The `LoadBalancerService` strategy publishes an ingress controller using a
Kubernetes [LoadBalancer
Service](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)
and on some platforms offers managed wildcard DNS.

![Image of LoadBalancerService](docs/images/endpoint-publishing-loadbalancerservice.png)

#### NodePortService

The `NodePortService` strategy publishes an ingress controller using a
Kubernetes [NodePort
Service](https://kubernetes.io/docs/concepts/services-networking/service/#nodeport).
With this strategy, the administrator is responsible for configuring
any external DNS or load balancer.

![Image of NodePortService](docs/images/endpoint-publishing-nodeportservice.png)

#### HostNetwork

The `HostNetwork` strategy uses host networking to publish the ingress
controller directly on the node host where the ingress controller is deployed.

![Image of HostNetwork](docs/images/endpoint-publishing-hostnetwork.png)

#### Private

The `Private` strategy does not publish the ingress controller.

![Image of Private](docs/images/endpoint-publishing-private.png)

## Troubleshooting

Use the `oc` command to troubleshoot operator issues.

To inspect the operator's status:

```shell
$ oc describe clusteroperators/ingress
```

To view the operator's logs:

```shell
$ oc logs --namespace=openshift-ingress-operator deployments/ingress-operator
```

To inspect the status of a particular ingress controller:

```shell
$ oc describe --namespace=openshift-ingress-operator ingresscontroller/<name>
```

## Contributing

Report issues in [Bugzilla](https://bugzilla.redhat.com/enter_bug.cgi?product=OpenShift%20Container%20Platform&component=Routing).

See [HACKING.md](HACKING.md) for development topics.
